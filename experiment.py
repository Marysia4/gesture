"""
Video Imitation Chain Demo
"""
import random
import shutil
import tempfile
from psynet.graphics import (
    GraphicPage,
    Frame,
    Text
)
import psynet.experiment
from psynet.consent import NoConsent
from psynet.media import download_from_s3, prepare_s3_bucket_for_presigned_urls
from psynet.modular_page import (
    AudioMeterControl,
    ModularPage,
    Prompt,
    VideoPrompt,
    VideoRecordControl,
)
from psynet.page import SuccessfulEndPage
from psynet.timeline import (
    Event,
    PreDeployRoutine,
    ProgressDisplay,
    ProgressStage,
    Timeline,
    join,
)
from psynet.trial.video import (
    CameraImitationChainNetwork,
    CameraImitationChainNode,
    CameraImitationChainSource,
    CameraImitationChainTrial,
    CameraImitationChainTrialMaker,
)
from psynet.utils import get_logger
import flask
logger = get_logger()

SILENT_RECORDING = "./static/5s_silence.wav"

import numpy as np
from psynet.graphics import (
    GraphicPage,
    Frame,
    Animation,
    Circle,
    Rectangle
)
lin = np.linspace(0, np.pi*2, 500)
a_list=[]

def lissajous(t, a, b, delta):
    return (np.sin(a * t + delta), np.sin(b * t))

def a_list_fun(a, b, delta):
    #return ((np.sin(a * t + delta)*1000), (np.sin(b * t)*1000))
    for i in range(500):
        a_list.append(Animation({"cx":(lissajous(lin, a, b, delta)[0][i])*400+500,"cy":(lissajous(lin, a, b, delta)[1][i])*400+500},
        duration=0.015))
    return a_list

class CustomNetwork(CameraImitationChainNetwork):
    __mapper_args__ = {"polymorphic_identity": "custom_network"}
    s3_bucket = "video-screen-recording-dev"


class CustomSource(CameraImitationChainSource):
    __mapper_args__ = {"polymorphic_identity": "custom_source"}

    def generate_seed(self, network, experiment, participant):
        #possibilities = ["Figure 8", "Circle", "Triangle", "Square"]
        #possibilities = ["beat1", "beat2","iconic1"]
        possibilities = [[1,2,0],[1,2,1/2],[1,2,1/4], [1,2,1],[1,3,0],[1,3,1],[2,3,1],[2,3,0]]
        return random.choice(possibilities)


class CustomVideoRecordControl(VideoRecordControl):
    def __init__(self):
        super().__init__(
            duration=5.0,
            s3_bucket="video-screen-recording-dev",
            recording_source="camera",
            audio_num_channels=2,
            controls=True,
            public_read=True,
            show_preview=True,
        )


class CustomProgressDisplay(ProgressDisplay):
    def __init__(self):
        super().__init__(
            stages=[
                ProgressStage([0.0, 1.5], "Get ready...", color="grey"),
                ProgressStage([1.5, 1.5 + 5.0], "Make your gesture!", color="red"),
                ProgressStage(
                    [1.5 + 5.0, 1.5 + 5.0],
                    "Click 'upload' if you're happy with your recording.",
                    color="green",
                    persistent=True,
                ),
            ],
        )


class CustomTrial(CameraImitationChainTrial):
    __mapper_args__ = {"polymorphic_identity": "custom_trial"}

    def show_trial(self, experiment, participant):
        if self.origin.degree == 1:
            instruction = f"Please trace out a lissajous: {self.origin.seed} in the air \
                            for the camera using you hands or fingers."
            #here we run a seed
            return join(
                    ModularPage(
                        "first time",
                        Prompt(text=instruction, text_align="center"),
                        time_estimate=5,
                    ),
                    GraphicPage(
                                label="animation",
                                dimensions=[1000, 1000],
                                #viewport_width=0.5,
                                time_estimate=10,
                                auto_advance_after=10,
                                frames=[
                                    Frame(
                                        [
                                            Rectangle('n', x=0, y=0, width=1000, height=1000, click_to_answer=False,
                                                      attributes={"fill": 'black', "opacity": 0.5}),

                                            Circle("logo",
                                                   x=(lissajous(lin, self.origin.seed[0], self.origin.seed[1], np.pi*self.origin.seed[2])[0][0])*400+500,
                                                   y=(lissajous(lin, self.origin.seed[0], self.origin.seed[1], np.pi*self.origin.seed[2])[1][0])*400+500,
                                                   radius=15, persist=False,
                                                   attributes={"fill": 'white', "opacity": 1}, loop_animations=False,
                                                   animations=a_list_fun(self.origin.seed[0], self.origin.seed[1], np.pi*self.origin.seed[2])
                                                   ),


                                        ], duration=10,
                                    ),
                                        Frame(
                                           [
                                           Text("number", "2", x=50, y=50)
                                        ], duration=10,
                                    ),

                                ],
                            ),
                    #ModularPage(
                       #"video",
                       # VideoPrompt(
                         #   f"/static/{self.origin.seed}.mp4",
                          #  "When you are ready, press next to imitate the gesture that you see.",
                           # muted=True,
                       # ),
                    #time_estimate=5,
                    #),
                    ModularPage(
                        "first time",
                        Prompt(text=instruction, text_align="center"),
                        CustomVideoRecordControl(),
                        time_estimate=5,
                        progress_display=CustomProgressDisplay(),
                        events={"recordStart": Event(is_triggered_by="trialStart", delay=1.5)},
                    ),
            )
           # )
        else:
            return join(
                [
                    ModularPage(
                        "subsequent-iteration-prompt",
                        VideoPrompt(
                            self.origin.target_url,
                            "When you are ready, press next to imitate the figure that you see.",
                            text_align="center",
                            width="360px",
                        ),
                        time_estimate=5,
                    ),
                    ModularPage(
                        "subsequent-iteration-record",
                        prompt="",
                        control=CustomVideoRecordControl(),
                        time_estimate=5,
                        progress_display=CustomProgressDisplay(),
                    ),
                ]
            )

    def analyze_recording(self, experiment, participant):
        return {"failed": False}


class CustomCameraImitationTrialMaker(CameraImitationChainTrialMaker):
    pass


class CustomNode(CameraImitationChainNode):
    __mapper_args__ = {"polymorphic_identity": "custom_node"}

    def summarize_trials(self, trials, experiment, participant):
        recording_info = trials[0].recording_info
        logger.info("RECORDING INFO: {}".format(recording_info))
        analysis = trials[0].analysis
        return dict(recording_info=recording_info, analysis=analysis)

    def synthesize_target(self, output_file):
        """
        This code can be modified to introduce custom video editing before reuploading the video.
        """
        if self.degree == 1:
            shutil.copyfile(SILENT_RECORDING, output_file)
            return output_file
        else:
            prev_trial = self.definition
            with tempfile.NamedTemporaryFile() as temp_file:
                recording = prev_trial["recording_info"]
                download_from_s3(
                    temp_file.name, recording["s3_bucket"], recording["key"]
                )
                # Processing of temp_file.name file.
                shutil.copyfile(temp_file.name, output_file)
                return output_file


####################################################################################################
class Exp(psynet.experiment.Experiment):
    timeline = Timeline(
        NoConsent(),
        PreDeployRoutine(
            "prepare_s3_bucket_for_presigned_urls",
            prepare_s3_bucket_for_presigned_urls,
            {
                "bucket_name": "video-screen-recording-dev",
                "public_read": True,
                "create_new_bucket": True,
            },
        ),
        ModularPage(
            "record_calibrate",
            """
            Please speak into your microphone and check that the sound is registered
            properly. If the sound is too quiet, try moving your microphone
            closer or increasing the input volume on your computer.
            """,
            AudioMeterControl(),
            time_estimate=5,
        ),
        CustomCameraImitationTrialMaker(
            id_="video-chain",
            network_class=CustomNetwork,
            trial_class=CustomTrial,
            node_class=CustomNode,
            source_class=CustomSource,
            phase="experiment",
            time_estimate_per_trial=15,
            chain_type="within",
            num_trials_per_participant=4,
            num_iterations_per_chain=4,
            num_chains_per_experiment=None,
            num_chains_per_participant=3,
            trials_per_node=1,
            balance_across_chains=False,
            recruit_mode="num_participants",
            check_performance_at_end=True,
            check_performance_every_trial=False,
            target_num_participants=25,
            wait_for_networks=True,
        ),
        SuccessfulEndPage(),
    )

    def __init__(self, session=None):
        super().__init__(session)
        self.initial_recruitment_size = 1
